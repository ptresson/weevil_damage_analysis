import os
from os import listdir
from os.path import isfile, join
from PIL import Image
import numpy as np
from scipy import ndimage
import matplotlib.pyplot as plt

"""

Simple python script to analyse pixels by color on images. 
Here the purpose is to extract the blue background (with get_mask_blue()),
then to analyse the proportion of black pixels vs. all non-blue pixels.
Here the black (damaged) pixels have been coverd with pure red to avoid artefacts.

"""


def img_frombytes(data):

    """
    convert a 0 and 1 matrix (data) into black and white image

    """
    size = data.shape[::-1]
    databytes = np.packbits(data, axis=1)
    return Image.frombytes(mode='1', size=size, data=databytes)




def get_mask(image, color="black",blur=False,blur_radius = 1.0,scale=1):

    """
    get all pixels from an image between min and max value of [R,G,B] (ex here with black, red and blue pixels).
    Image can be blured or scaled to get rid of artefacts.
    returns the 0 and 1 mask of pixels corresponding to this threshold and number of pixels.

    """

    if color == "black":
        color_array = np.array([[25, 25, 25],[0, 0, 0]])  # black
    
    if color == "red":
        color_array = np.array([[256, 2, 2],[254, -1, -1]])  # pure red
    
    if color == "darkblue":
        color_array = np.array([[40, 60, 100],[0, 0, 25]])  # dark blue
    
    im = Image.open(image)
    width, height = im.size
    width2 = int(width*scale)
    height2 = int(height*scale)
    im2 = im.resize((width2,height2), Image.ANTIALIAS)
    if blur :
         im2 = ndimage.gaussian_filter(im2, blur_radius)

    
    im2 = np.asarray(im2)
    
    
    mask = np.all(
        np.logical_and(
            np.min(color_array, axis=0) <= im2,
            im2 <= np.max(color_array, axis=0)
        ),
        axis=-1
    )

    num_px = np.sum(mask)

    return num_px, mask


def get_mask_blue(image,scale=1):
    
    """
    get all pixels from an image with B>G>R (blue).
    Image can be scaled to get rid of artefacts.
    returns the 0 and 1 mask of pixels corresponding to this threshold and number of pixels.

    """
    
    im = Image.open(image)
    width, height = im.size
    width2 = int(width*scale)
    height2 = int(height*scale)
    im2 = im.resize((width2,height2), Image.ANTIALIAS)


    im2 = np.asarray(im2)

    mask = np.logical_and(
            im2[:,:,0] < im2[:,:,1], # select pixels where Green is greater than Red
            im2[:,:,1] < im2[:,:,2] # and Blue greater than Green
        )

    # print(mask)
    num_px = np.sum(mask)

    return num_px, mask




origine = os.path.dirname(os.path.abspath(__file__)) # if your data is in same folder as the script


# open recap csv file
list_file = open("recap.csv","w")
list_file.write("image,num_banana,num_black,prop_degats\n") # column names


# if several folders containing images to analyse.
# dir_list = ['bulbes_verif/LP','bulbes_verif/SL','bulbes_verif/BM','bulbes_verif/PC','bulbes_verif/PE']
dir_list = [origine]

for origine in dir_list :
    
    print(origine)
    or_dir = os.listdir(origine)

    # get rid of previous atempts
    for item in or_dir:
        # print(item)
        if (item.endswith('_mask.jpg')):
            os.remove(os.path.join(origine, item))

    # check that your images do end up with '.JPG', otherwise, change the extension here
    files_to_list = sorted([f for f in os.listdir(origine) if f.endswith('.JPG')]) 
    
    # variables to see the progression
    files_tot = len(files_to_list)
    file_current = 1
    n_image = 0

    # buffer_id_global = 'start'
    num_black_buffer = 0
    num_banana_buffer = 0
    
    
    for image in files_to_list :
    
        image = origine + '/' + image
        n_image =+1
        print("%s (%d/%d)" %(image,file_current,files_tot))
        file_current +=1
        
        im = Image.open(image)
        width, height = im.size

        num_tot = width * height
        # here I use corrected images with red colored blakc pixels. that is why num_black and mask_black call the 'red' color
        num_black, mask_black = get_mask(image, color="red") 
        num_blue, mask_blue = get_mask_blue(image)
        num_banana = num_tot - num_blue # all non-blue pixels

        img_mask_black = img_frombytes(mask_black)
        img_mask_black.save(str(image[:-4])+ '_black_mask.jpg') # save the mask image to check visually as IMAGENAME_black_mask.jpg

        img_mask_blue = img_frombytes(mask_blue)
        img_mask_blue.save(str(image[:-4])+ '_blue_mask.jpg') # idem with blue mask

        # for general use
        prop_damage = float(num_black / num_banana)
        list_file.write("%s,%d,%d,%4f\n" % (image,num_banana,num_black,prop_damage))

list_file.close()
